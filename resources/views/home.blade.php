@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            @guest
                <p>Войдите в личный кабинет, чтобы оставить комментарий</p>
            @else
                <p>Вы млжете оставить комментарий или отредактировать свои комментарии</p>
            @endguest
        </div>
    </div>
    <comments-component @guest :user="'guest'" @else :user="'{{ Auth::user() }}'" @endguest></comments-component>
</div>
@endsection
